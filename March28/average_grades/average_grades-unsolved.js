/*
  input=array of objects containing student names
  write js fxn calcs average score
  output: return object mapping each student name to average score
*/



function averageGrades(grades, scoreMap = {}) {
  for (let grade of grades) {
    if (scoreMap[grade.name]) {
      scoreMap[grade.name].push(grade.grade)
    } else {
      scoreMap[grade.name] = [grade.grade]
    }
  }
  for (const name in scoreMap) {
    const grades = scoreMap[name]
    const average = grades.reduce((a, b) => {
      return (a + b) / grades.length
    }
    )
    scoreMap[name] = average
  }
  return scoreMap
}

const grades = [
  { name: "Alice", grade: 75 },
  { name: "Bob", grade: 80 },
  { name: "Alice", grade: 85 },
  { name: "Bob", grade: 90 },
  { name: "Charlie", grade: 70 },
  { name: "Alice", grade: 90 },
  { name: "Charlie", grade: 80 },
];

console.log(averageGrades(grades));

module.exports = averageGrades;
