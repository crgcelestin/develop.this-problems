function flattenArray(arr, flat = []) {
  for (let num of arr) {
    if (typeof num === 'object') {
      flattenArray(num, flat)
    } else {
      flat.push(num)
    }
  }
  return flat
}

const arr1 = [1, [2, 3], 4, [[5, 6], 7]];
console.log(flattenArray(arr1));

module.exports = flattenArray;
