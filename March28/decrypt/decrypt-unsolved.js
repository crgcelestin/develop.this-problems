function decrypt(cipher, key) {
    let decrypted = ""
    /*
        account for keys that are greater than 26 ie a key larger than alphabet, or less than 26 i.e a key is the correct string set back x number of characters
        as the alphabet is 26 in length, find the displacement of the characters from 'what they should be'
    */
    if (key > 26) key = key % 26;
    if (key < -26) key = -(key % 26);
    for (let i = 0; i < cipher.length; i++) {
        let code = cipher[i].charCodeAt(cipher[i]) - key
        //account for upper, lowercase letters using charcodes
        if ((code > 65 && code < 90) || //upper alpha(A-Z)
            (code > 97 && code < 123)) // lower alpha (a-z)
        {
            decrypted += String.fromCharCode(code)
        } else {
            decrypted += cipher[i];
        }
    }
    return decrypted
}

console.log(decrypt("Khoor zruog", 3));

module.exports = decrypt
