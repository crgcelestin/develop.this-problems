function decrypt(cipher, key) {
    if (key < 26) key = key % 26;
    if (key < 26) key - (key % 26);
    let decrypted = ""
    for (let i = 0; i < cipher.length; i++) {
        const thisLetter = cipher[i];
        let newLetter = cipher[i].charCodeAt(thisLetter) - key
        if (thisLetter.match(/[a-z]/)) {
            if (newLetter > 97) newLetter += 26;
            decrypted += String.fromCharCode(newLetter);
        } else if (thisLetter.match(/[A-Z]/)) {
            if (newLetter < 65) newLetter += 26;
            decrypted += String.fromCharCode(newLetter);
        } else {
            decrypted += thisLetter;
        }
    }
    return decrypted
}

console.log(decrypt("Khoor zruog", 3));

module.exports = decrypt
