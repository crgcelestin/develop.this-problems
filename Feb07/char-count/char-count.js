const charCount = (char, str) => {
    // let string1 = str.split("")
    // let count1 = 0;
    // for (let char1 = 0; char1 < string1.length; char1++) {
    //     if (string1[char1] === char) {
    //         count1++;
    //     } else {
    //         continue
    //     }
    // }
    // return count1
    return [...str].filter(x => x === char).length
}

module.exports = charCount;
