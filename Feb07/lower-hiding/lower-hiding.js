const detectWord = (str) => {
    // Your code here

    // return [...str].filter(word => word === word.toLowerCase()).join("")
    let stringarray = str.split("")
    let newarray = [];
    for (let arr = 0; arr < stringarray.length; arr++) {
        if (stringarray[arr] === stringarray[arr].toLowerCase()) {
            newarray.push(stringarray[arr])
        }
    }
    return newarray.join("")
}

module.exports = detectWord;
