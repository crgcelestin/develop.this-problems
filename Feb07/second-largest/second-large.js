const secondLargest = (arr) => {
    // Your code here
    // const firstLargest = (arr1) => {
    //     let largest = arr1[0]
    //     for (let arrLarge = 0; arrLarge < arr1.length; arrLarge++) {
    //         if (arr1[arrLarge] > largest) {
    //             largest = arr1[arrLarge]
    //         }
    //     }
    //     return largest
    // }
    // let firstLargest1 = firstLargest(arr);
    // let firstLargestIndex = arr.indexOf(firstLargest1);
    // arr.splice(firstLargestIndex, 1)
    // return firstLargest(arr)
    arr.splice(arr.indexOf(Math.max(...arr)), 1);
    return Math.max(...arr)
}

module.exports = secondLargest;
