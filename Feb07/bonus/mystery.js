const mysteryFunction = (a, b) => {
	const aggregate = (a) => {
		let sum = 0
		const num_str = a.toString()
		for (let num of num_str) {
			sum += parseInt(num)
		}
		return sum
	}
	return aggregate(a) * aggregate(b)
	// console.log(var_string, typeof var_string)
}

console.log(mysteryFunction(10, 10))

module.exports = mysteryFunction;
