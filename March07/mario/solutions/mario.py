# input: number of coints
# ouput: return obj with converted number of lives, remaining coins

"""
100 coins are = 1 life
x coins
example: 150 coins
first do a check
when num is dividied by 100 is there a remainder using modulo
if it is divisble, find the remainder
150%100 = 50

250
2

"""
import math


def coinConverter(coins):
    # mario = {}
    # if coins >= 100:
    #     mario["lives"] = math.floor(coins / 100)
    #     mario["coinsRemaining"] = coins % 100
    # else:
    #     mario["lives"] = 0
    #     mario["coinsRemaining"] = coins
    return {
        "lives": math.floor(coins / 100),
        "coinsRemaining": coins % 100,
    }
