const coinConverter = (coins) => {
    return {
        "lives": Math.floor(coins / 100),
        "coinsRemaining": coins % 100,
    };
}

module.exports = coinConverter;
