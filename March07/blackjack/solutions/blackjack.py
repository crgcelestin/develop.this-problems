# input: set of string values representing cards
# step 1 - convert both cards to integer equivalent
# 1(a) convert face cards to 10, return
# 1(b) convert A to 11, return
# 1(c) else convert all to integers
# step 2 - evaluate if sum of both cards is less or equal to 14
# output: return 1 if total<=14 else return 0


def get_card_values(card):
    if card in ["J", "Q", "K"]:
        return 10
    elif card == "A":
        return 11
    return int(card)


def doIHit(card1, card2):
    if get_card_values(card1) + get_card_values(card2) <= 14:
        return 1
    return 0
