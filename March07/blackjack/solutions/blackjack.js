const doIHit = (card1, card2) => {
    /*
    using switch can return an output based on the
    evaluation of input parameter
        first scenario is that its a face card with a value less tha that of A, return 10 for card matching any of those cases
        second scenario is face card with value = 'A', return 11
        else return integer value of inputted card param
    */
    function getValue(card) {
        switch (card) {
            case "J":
            case "Q":
            case "K":
                return 10;
            case "A":
                return 11;
            default:
                return parseInt(card);
        }
    }
    return getValue(card1) + getValue(card2) <= 14 ? 1 : 0;
}

module.exports = doIHit;
