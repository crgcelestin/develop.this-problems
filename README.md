# Develop.this Problems
- Coding Problems from the Develop.This() Discord

| Date | Problem | Problem README | Problem Solution(s)
| --- | --- | --- | --- |
March 28, 2023 | Average Grades | [Readme](March28/average_grades/README.md)| [Solution](March28/average_grades/average_grades-unsolved.js)|
... | Decrypt | [Readme](March28/decrypt/README.md)|[Solution](March28/decrypt/decrypt-solution.js)|
... | Flatten Array | [Readme](March28/flatten_array/README.md) | [Solution](March28/flatten_array/flatten_array.test.js) |
March 14, 2023 | Arithmetic | [Readme](March14/problems/arithmetic/README.md) |[Solution](March14/problems/arithmetic/README.md)|
... | Calculate Age | [Readme](March14/problems/calculate_age/README.md) | [Solution](March14/problems/calculate_age/calculate_age.js) |
... | No Vowels | [Readme](March14/problems/no-vowels/README.md)| [Solution](March14/problems/no-vowels/no-vowels.js)
... | Reverse String | [Readme](March14/problems/reverse-string/README.md) | [Solution](March14/problems/reverse-string/reverse-string.js)
March 7, 2023 | Mario| [Readme](March07/mario/README.md) | [Solutions](March07/mario/solutions) |
... | BlackJack|[Readme](March07/blackjack/README.md) | [Solutions](March07/blackjack/solutions) |
Feb 7, 2023 | Char Count | [Readme](Feb07/char-count/char-count.js)|[Solution](Feb07/char-count/char-count.js)|
... | Lower Hiding | [Readme](Feb07/lower-hiding/README.md) |[Solution](Feb07/lower-hiding/lower-hiding.js) |
... | Minutes to Seconds | [Readme](Feb07/minutes-seconds/minutes-seconds.test.js) |[Solution](Feb07/minutes-seconds/minutes-seconds.js) |
... | Second Largest | [Readme](Feb07/second-largest/README.md) | [Solution](Feb07/second-largest/second-large.js)|
... | Time | [Readme](Feb07/time/README.md)| [Solution](Feb07/time/time.js)|
... | Mystery | [Readme](Feb07/bonus/README.md) | [Solution](Feb07/bonus/mystery.js) |
