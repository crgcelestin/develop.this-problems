/*
input: date of birth
CLI argument in format:
    year-monthNumber-dayNumber

function:
    (1)
        create a date object with the given birthdate using New Date
        birthDate=new Date(input)
    (2)
        eval the output of the Date object
    (3)
        new Date()
        is already today/Now
    (4)
        using Math.abs(today-birth)
        returns difference in milliseconds
    (5)
        convert mililiseconds to years
        get arithmetic value of miliseconds in days * 365
        Math.floor(milis diff -> years diff)
    (6)

output: 'You are `{x}` years old'
x = current day - birth date (difference between the current time and date of starting existence)
24 = 2023 (est) - 1999 (est)
    02 10            02 10

*/

function calculateAge(birthdayString) {
    const birthDate = new Date(birthdayString)
    console.log(birthDate)
    if (birthDate instanceof Date && !isNaN(birthDate.valueOf())) {
        const todayDate = new Date()
        var day = (1000 * 60 * 60 * 24 * 365)
        const difference = todayDate - birthDate
        const age = Math.floor(difference / day)
        return age
    }
    throw new Error('Invalid date string')
}

console.log(calculateAge())

module.exports = calculateAge;
