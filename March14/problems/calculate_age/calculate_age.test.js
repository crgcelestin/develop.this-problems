const calculateAge = require("./calculate_age.js");
// const calculateAge = require('../../solutions/calculate_age');
const Error = 'Invalid date string'
describe("calculateAge function", () => {
  test("returns age in years for a given birth date string", () => {
    const birthdayString = "1990-03-01";
    const expectedAge = 33;

    expect(calculateAge(birthdayString)).toBe(expectedAge);
  });

  test("returns age in years for a different birth date string", () => {
    const birthdayString = "2000-01-01";
    const expectedAge = 23;

    expect(calculateAge(birthdayString)).toBe(expectedAge);
  });

  test("throws an error for an invalid birth date string", () => {
    const birthdayString = "2000-01-01-01";
    expect(() =>
      birthdayString.valueOf()).toThrowError(Error);
  });

  test("calculates age accurately for birth dates later than current date", () => {
    const birthdayString = "2050-01-01";
    const expectedAge = -27;

    expect(calculateAge(birthdayString)).toBe(expectedAge);
  });
});
