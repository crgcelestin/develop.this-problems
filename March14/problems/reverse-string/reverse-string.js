const reverseString = (string) => {
  if (!string) return "";
  if (string.length === 1) return string;
  return string.slice(-1) + reverseString(string.slice(0, -1))
};

console.log(reverseString("Hello, world!"))

module.exports = reverseString;

/*
*/
