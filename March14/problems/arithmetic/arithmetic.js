/*
input: two integers
1 case:
    x/y=a
    a=quotient
    if quotient involves division by 0, set
    quotient property
    to cannot 'divide by zero'
    else return value u[ to three decimal places
output: display sum, difference, product and quotient of numbers
*/


// class a1rithmetic {
//     constructor(num1, num2) {
//         this.array = [num1, num2]
//     }
//     isDivisible(num) {
//         return this.array.some((arrayNum) => {
//             return arrayNum * 2
//         });
//     }
// }
// const test = new a1rithmetic([1, 2])
// console.log(test.isDivisible(1, 2))


const arithmetic = (num1, num2, arithmetic1 = {}) => {
    const sum_function = (num1, num2) => {
        arithmetic1['sum'] = num1 + num2
    }
    const difference_function = (num1, num2) => {
        arithmetic1['difference'] = num1 - num2
    }
    function product_function(num1, num2) {
        arithmetic1['product'] = (num1 * num2)
    }
    function division_function(num1, num2) {
        if (num2 === 0) arithmetic1['quotient'] = 'cannot divide by zero';
        else arithmetic1['quotient'] = Number((num1 / num2).toFixed(3));
    }
    sum_function(num1, num2);
    difference_function(num1, num2);
    product_function(num1, num2);
    division_function(num1, num2);
    return arithmetic1;
}
console.log(arithmetic(20, 4));

module.exports = arithmetic;
