const noVowels = (string, arr = ["a", "e", "i", "o", "u", "y"]) => {
  if (!string) return "";

  var replace = [];
  for (let i = 0; i < string.length; i++) {
    if (arr.includes(string[i].toLowerCase())) {
      replace.push("x")
    } else {
      replace.push(string[i])
    }
  }
  return replace.join("")
};
console.log(noVowels("The rain in Spain falls mainly on the plain"))

module.exports = noVowels;
